<?php
class Database
{
	public $dbh;

	public function __construct() {
		try {
			$config = Router::config('database');
			$this->dbh = new PDO('mysql:host=' . $config['host'] . ';dbname=' . $config['database'], $config['user'], $config['password']);
		} catch (Exception $e) {
			print "Error!: " . $e->getMessage();
			die();
		}
	}

	public function getConnection() {
		return $this->dbh;
	}
}